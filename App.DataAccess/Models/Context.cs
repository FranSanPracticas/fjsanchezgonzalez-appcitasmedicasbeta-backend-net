﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DataAccess.Models
{
    public class Context : DbContext
    {

        public Context()
        {
        }

        public Context(DbContextOptions<Context> options)
            : base(options)
        {
        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Cita> Citas { get; set; }
        public DbSet<Paciente> Pacientes { get; set; }
        public DbSet<Medico> Medicos { get; set; }

        public DbSet<Diagnostico> Diagnosticos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseOracle("User Id=STUDIO;Password=STU;Data Source=localhost:1521/orcl;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>()

                .HasDiscriminator<int>("UsuarioType")

                .HasValue<Usuario>(1)

                .HasValue<Paciente>(2)
            .HasValue<Medico>(3);


            modelBuilder.Entity<Cita>()
            .HasOne(a => a.CitaPaciente)
            .WithOne(a => a.PacienteCitas)
            .HasForeignKey<Paciente>(c => c.Id);

            modelBuilder.Entity<Cita>()
            .HasOne(a => a.CitaMedico)
            .WithOne(a => a.MedicoCitas)
            .HasForeignKey<Medico>(c => c.Id);

            modelBuilder.Entity<Cita>()
            .HasOne(a => a.CitaDiagnostico)
            .WithOne(a => a.DiagnosticoCita)
            .HasForeignKey<Diagnostico>(c => c.Id);

            modelBuilder.Entity<Medico>()
            .HasOne(a => a.MedicoPacientes)
            .WithOne(a => a.PacienteMedicos)
            .HasForeignKey<Paciente>(c => c.Id);




        }

    }
}
