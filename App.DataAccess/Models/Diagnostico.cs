﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DataAccess.Models
{
    public class Diagnostico
    {

        public int Id { get; set; }

        public string ValoracionEspecialista { get; set; }
        public string Enfermedad { get; set; }

        public Cita DiagnosticoCita { get; set; }

    }
}
