﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DataAccess.Models
{
    public class Cita
    {
        public int Id { get; set; }
        public string MotivoCita { get; set; }

        public DateTime FechaHora { get; set; }

        public Paciente CitaPaciente { get; set; }

        public Medico CitaMedico { get; set; }

        public Diagnostico CitaDiagnostico { get; set; }
    }
}