﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DataAccess.Models
{
    public class Paciente : Usuario
    {
        public string Nss { get; set; }
        public string NumTarjeta { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }


        public Cita PacienteCitas { get; set; }
        public Medico PacienteMedicos { get; set; }

    }
}
