﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DataAccess.Models
{
    public class Medico : Usuario
    {



        public string NumColegiado { get; set; }
        public Cita MedicoCitas { get; set; }
        public Paciente MedicoPacientes { get; set; }

    }
}
