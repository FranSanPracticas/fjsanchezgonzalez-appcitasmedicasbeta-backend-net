﻿using App.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AppCitasMedicas_backendV4.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitaController : ControllerBase
    {

        private Context _context;

        public CitaController(Context context)
        {
            _context = context;
        }


        [HttpGet]
        public async Task<List<Cita>> Listar()
        {
            return await _context.Citas.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Cita>> BuscarPorId(decimal id)
        {
            var retorno = await _context.Citas.FirstOrDefaultAsync(x => x.Id == id);

            if (retorno != null)
                return retorno;
            else
                return NotFound();
        }

        [HttpPost]
        public async Task<ActionResult<Cita>> Guardar(Cita c)
        {
            try
            {
                await _context.Citas.AddAsync(c);
                await _context.SaveChangesAsync();
                c.Id = await _context.Citas.MaxAsync(u => u.Id);

                return c;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }

        [HttpPut]
        public async Task<ActionResult<Cita>> Actualizar(Cita c)
        {
            if (c == null || c.Id == 0)
                return BadRequest("Faltan datos");

            Cita cat = await _context.Citas.FirstOrDefaultAsync(x => x.Id == c.Id);

            if (cat == null)
                return NotFound();

            try
            {
                cat.MotivoCita = c.MotivoCita;
                cat.FechaHora = c.FechaHora;
                cat.CitaPaciente = c.CitaPaciente;
                cat.CitaMedico = c.CitaMedico;
                cat.CitaDiagnostico = c.CitaDiagnostico;
                _context.Citas.Update(cat);
                await _context.SaveChangesAsync();

                return cat;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Eliminar(decimal id)
        {
            Cita cat = await _context.Citas.FirstOrDefaultAsync(x => x.Id == id);

            if (cat == null)
                return NotFound();

            try
            {
                _context.Citas.Remove(cat);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }


    }
}
