﻿using App.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AppCitasMedicas_backendV4.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PacienteController : ControllerBase
    {


        private Context _context;

        public PacienteController(Context context)
        {
            _context = context;
        }


        [HttpGet]
        public async Task<List<Paciente>> Listar()
        {
            return await _context.Pacientes.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Paciente>> BuscarPorId(decimal id)
        {
            var retorno = await _context.Pacientes.FirstOrDefaultAsync(x => x.Id == id);

            if (retorno != null)
                return retorno;
            else
                return NotFound();
        }

        [HttpPost]
        public async Task<ActionResult<Paciente>> Guardar(Paciente c)
        {
            try
            {
                await _context.Pacientes.AddAsync(c);
                await _context.SaveChangesAsync();
                c.Id = await _context.Pacientes.MaxAsync(u => u.Id);

                return c;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }

        [HttpPut]
        public async Task<ActionResult<Paciente>> Actualizar(Paciente c)
        {
            if (c == null || c.Id == 0)
                return BadRequest("Faltan datos");

            Paciente cat = await _context.Pacientes.FirstOrDefaultAsync(x => x.Id == c.Id);

            if (cat == null)
                return NotFound();

            try
            {
                cat.Nss = c.Nss;
                cat.NumTarjeta = c.NumTarjeta;
                cat.Telefono = c.Telefono;
                cat.Direccion = c.Direccion;
                cat.PacienteMedicos = c.PacienteMedicos;
                cat.PacienteCitas = c.PacienteCitas;
                _context.Pacientes.Update(cat);
                await _context.SaveChangesAsync();

                return cat;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Eliminar(decimal id)
        {
            Paciente cat = await _context.Pacientes.FirstOrDefaultAsync(x => x.Id == id);

            if (cat == null)
                return NotFound();

            try
            {
                _context.Pacientes.Remove(cat);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }



    }
}
