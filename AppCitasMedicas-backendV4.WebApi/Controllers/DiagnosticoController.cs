﻿using App.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AppCitasMedicas_backendV4.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiagnosticoController : ControllerBase
    {

        private Context _context;

        public DiagnosticoController(Context context)
        {
            _context = context;
        }


        [HttpGet]
        public async Task<List<Diagnostico>> Listar()
        {
            return await _context.Diagnosticos.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Diagnostico>> BuscarPorId(decimal id)
        {
            var retorno = await _context.Diagnosticos.FirstOrDefaultAsync(x => x.Id == id);

            if (retorno != null)
                return retorno;
            else
                return NotFound();
        }

        [HttpPost]
        public async Task<ActionResult<Diagnostico>> Guardar(Diagnostico c)
        {
            try
            {
                await _context.Diagnosticos.AddAsync(c);
                await _context.SaveChangesAsync();
                c.Id = await _context.Diagnosticos.MaxAsync(u => u.Id);

                return c;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }

        [HttpPut]
        public async Task<ActionResult<Diagnostico>> Actualizar(Diagnostico c)
        {
            if (c == null || c.Id == 0)
                return BadRequest("Faltan datos");

            Diagnostico cat = await _context.Diagnosticos.FirstOrDefaultAsync(x => x.Id == c.Id);

            if (cat == null)
                return NotFound();

            try
            {
                cat.ValoracionEspecialista = c.ValoracionEspecialista;
                cat.Enfermedad = c.Enfermedad;
                cat.DiagnosticoCita = c.DiagnosticoCita;
                _context.Diagnosticos.Update(cat);
                await _context.SaveChangesAsync();

                return cat;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Eliminar(decimal id)
        {
            Diagnostico cat = await _context.Diagnosticos.FirstOrDefaultAsync(x => x.Id == id);

            if (cat == null)
                return NotFound();

            try
            {
                _context.Diagnosticos.Remove(cat);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }

    }
}
