﻿using App.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AppCitasMedicas_backendV4.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicoController : ControllerBase
    {

        private Context _context;

        public MedicoController(Context context)
        {
            _context = context;
        }


        [HttpGet]
        public async Task<List<Medico>> Listar()
        {
            return await _context.Medicos.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Medico>> BuscarPorId(decimal id)
        {
            var retorno = await _context.Medicos.FirstOrDefaultAsync(x => x.Id == id);

            if (retorno != null)
                return retorno;
            else
                return NotFound();
        }

        [HttpPost]
        public async Task<ActionResult<Medico>> Guardar(Medico c)
        {
            try
            {
                await _context.Medicos.AddAsync(c);
                await _context.SaveChangesAsync();
                c.Id = await _context.Medicos.MaxAsync(u => u.Id);

                return c;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }

        [HttpPut]
        public async Task<ActionResult<Medico>> Actualizar(Medico c)
        {
            if (c == null || c.Id == 0)
                return BadRequest("Faltan datos");

            Medico cat = await _context.Medicos.FirstOrDefaultAsync(x => x.Id == c.Id);

            if (cat == null)
                return NotFound();

            try
            {
                cat.NumColegiado = c.NumColegiado;
                cat.MedicoPacientes = c.MedicoPacientes;
                cat.MedicoCitas = c.MedicoCitas;
                _context.Medicos.Update(cat);
                await _context.SaveChangesAsync();

                return cat;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Eliminar(decimal id)
        {
            Medico cat = await _context.Medicos.FirstOrDefaultAsync(x => x.Id == id);

            if (cat == null)
                return NotFound();

            try
            {
                _context.Medicos.Remove(cat);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "Se encontró un error");
            }
        }

    }
}
